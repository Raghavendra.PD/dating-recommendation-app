import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/*
 * This is the main class. Invoke this to start the app.
 * 
 * USAGE:
 * 		- Initially the the class asks for user input for following operations
 * 				[1] Add user details
 * 				[2] Get dating recommendations
 * 				[3] Remove user details
 * 				[4] Display user details already entered
 * 				[5] Exit app.
 * 
 * 		- To add user details (operation 1 mentioned above), user name, age, gender and
 * 			 interests to be entered in following format: <user name>-<age>-<gender>-<interests>
 * 			Example: UserA-25-Female-Cricket,Tennis
 * 				
 * 		- To get dating recommendations (operation 2 mentioned above), user name and number
 * 			of recommendations need to be entered.
 * 
 * 		- To remove user details already entered (operation 3 mentioned above), user name 
 * 			needs to be entered.
 * 
 */
public class DatingRecommendationApp {

	public static void main(String[] args) {
				
		do {
			// Request operation i.e Add user details / Get dating recommendation / remove user details / Exit
			System.out.print(Constants.OPERATION_INPUT_REQUEST_MESSAGE);;

			String operationInput = getConsoleInput();
			Integer operation = null;
			
			try {
				operation = Integer.valueOf(operationInput);
			} catch(NumberFormatException e) {
				System.out.print("ERROR: Invalid number entered.\n");
				continue;
			}
			
			switch(operation) {
			case Constants.OPERATION_ADD_USER_DETAILS:
				addUserDetailsHandler();
				break;
			case Constants.OPERATION_GET_DATING_RECOMMENDATION:
				datingRecommendationHandler();
				break;
			case Constants.OPERATION_REMOVE_USER_DETAILS:
				removeUserDetailsHandler();
				break;
			case Constants.OPERATION_DISPLAY_USER_DB:
				displayUserDBHandler();
				break;
			case Constants.OPERATION_EXIT_APP:
				System.exit(0);
				break;
			default:
				System.out.print("ERROR: Invalid number entered.\n");
			}
		} while (true);

	}
	
	/* *************************************************************************
	 * Helper methods to be used internally DatingRecommendationApp Class
	 * *************************************************************************
	 *
	 * Helper Method: This method handles user input for adding user details.
	 * 
	 */
	private static void addUserDetailsHandler() {
		
		// Request user to add user details
		System.out.print(Constants.FIRST_USER_INPUT_REQUEST_MESSAGE);
		
		do {
			String nextInput = getConsoleInput();
			if (0 == nextInput.compareToIgnoreCase(Constants.END_OF_USER_INPUT)) {
				break;
			}
	
			List<String> errorMessages = new ArrayList<String>();
			UserDetails userDetails = UserDetailsCollection.validateUserInputAndCreateUserDetails(nextInput, errorMessages);
			
			if(null == userDetails) {
				System.out.print("ERROR: Wrong user input. Details(s)given below:\n");
				for(String errorMessage: errorMessages) {
					System.out.print(" - " + errorMessage + "\n");
				}
			} else {
				UserDetailsCollection.addUserDetails(userDetails);
			}
			
			System.out.print(Constants.NEXT_USER_INPUT_REQUEST_MESSAGE);
			
		} while (true);
	}
	
	/*
	 * Helper Method: This method handles user input for providing dating recommendation.
	 */
	private static void datingRecommendationHandler() {
		// Request user to enter user name
		System.out.print(Constants.DATING_RECOMMENDATION_INPUT_MESSAGE);
		
		String userName = getConsoleInput();
		if(false == UserDetailsCollection.isUserDetailsAvailable(userName)) {
			System.out.print("ERROR: Invalid user name.\n");
			return;
		}
		System.out.print(Constants.DATING_RECOMMENDATION_INPUT_MESSAGE_NO_OF_RECOMM);
		
		String numberOfRecommendations = getConsoleInput();
		int nNumberOfRecommendations = 0;
		try {
			nNumberOfRecommendations = Integer.valueOf(numberOfRecommendations);
		} catch(NumberFormatException e) {
			System.out.print("ERROR: Invalid number entered.\n");
			return;
		}
		
		List<String> recommendations = UserDetailsCollection.getDatingRecommendations(userName, nNumberOfRecommendations);
		System.out.print(Constants.INTERMEDIATE_LINE_BREAK);
		System.out.print("Dating recommendations for " + userName + ":\n");
		if(recommendations.size() >= 1) {
			for(String recommendation: recommendations) {
				System.out.print(recommendation + "\n");
			}
		} else {
			System.out.print("<NONE>\n");
		}

		System.out.print(Constants.INTERMEDIATE_LINE_BREAK);
	}
	
	/*
	 * Helper Method: This method handles user input for removing user details.
	 */
	private static void removeUserDetailsHandler() {
		// Request user to enter user name
		System.out.print(Constants.USER_REMOVAL_INPUT_MESSAGE);
		String userName = getConsoleInput();
		if(false == UserDetailsCollection.isUserDetailsAvailable(userName)) {
			System.out.print("ERROR: Invalid user name.\n");
			return;
		} else {
			UserDetailsCollection.removeUserDetails(userName);
		}
	}
	
	/*
	 * Helper Method: This method handles user input for displaying existing user details.
	 */
	private static void displayUserDBHandler() {
		System.out.print(Constants.INTERMEDIATE_LINE_BREAK);
		for(UserDetails userDetails: UserDetailsCollection.getAllUserDetails()) {
			System.out.print(userDetails.toString());
			System.out.print(Constants.INTERMEDIATE_LINE_BREAK);
		}		
	}
	
	/*
	 * Helper Method: This method reads user input line on console.
	 */
	private static String getConsoleInput() {
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		String newLine = "";
		try {
			newLine = bufferedReader.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return newLine;
	}
}
