import java.util.List;

/*
 * This class models user details. Actual logic of storing, retrieving user details and 
 * generating dating recommendations are covered in UserDetailsCollection class.
 */
public class UserDetails {
	
	/*
	 * Constructor and getter/setter methods.
	 */
	public UserDetails(String userName, Integer age, String gender, List<String> interests) {
		super();
		this.userName = userName;
		this.age = age;
		this.gender = gender;
		this.interests = interests;
	}
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public List<String> getInterests() {
		return interests;
	}

	public void setInterests(List<String> interests) {
		this.interests = interests;
	}
	
	/*
	 * Override toString to display contents of User Details.
	 */
	@Override
    public String toString() { 
		String returnString = "";
		returnString += "User name: " + userName + "\n";
		returnString += "Age: " + age + "\n";
		returnString += "Gender: " + gender + "\n";
		returnString += "Interests: " + interests.toString() + "\n";		
		
        return returnString; 
    } 
	
	// Member variables
	private String userName;
	private Integer age;
	private String gender;
	private List<String> interests = null;

}
