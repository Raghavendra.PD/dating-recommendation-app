import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

/*
 * This class models data structures to store multiple User Details. Provides public 
 * methods to validate user input, add/remove user details to collection and to 
 * generate dating recommendations for a given user.
 */
public class UserDetailsCollection {
	
	/* *************************************************************************
	 * Public methods to be used by DatingRecommendationApp Class
	 * *************************************************************************
	 * 
	 * This method performs validations for user details input.
	 * 
	 */
	public static UserDetails validateUserInputAndCreateUserDetails(String userInputLine, List<String> errorMessages) {
		
		// Check for empty string.
		if(userInputLine.isEmpty()) {
			errorMessages.add("Empty user input.\n");
			return null;
		}
		
		// Check if all 4 details for a user is provided
		String[] userDetailsStrings = userInputLine.split("-");
		if(userDetailsStrings.length != 4) {
			errorMessages.add("Invalid number of user details.\n");
			return null;
		}
		
		// Check if age field is a number
		Integer age = 0;
		try {
			age = Integer.valueOf(userDetailsStrings[1]);
			
		}catch(NumberFormatException e) {
			errorMessages.add("Age is not mentioned as number.\n");
		}
		
		if(age < Constants.MINIMUM_AGE_FOR_DATING) {
			errorMessages.add("Age must be more than minimum age for dating i.e, "
								+ Constants.MINIMUM_AGE_FOR_DATING + " years. \n");
		}
		
		// Check if gender field is Male / Female
		String gender = userDetailsStrings[2];
		if(! (gender.equals(Constants.GENDER_MALE) || gender.equals(Constants.GENDER_FEMALE))) {
			errorMessages.add("Gender is not vailid value.\n");
		}
		
		if(!errorMessages.isEmpty()) {
			return null;
		}
		
		String interestString = userDetailsStrings[3];
		String[] interestStringArray = interestString.split(Constants.INTERESTS_INPUT_DELIMITER);
		List<String> interestList = new ArrayList<String>();
		for(String interest: interestStringArray) {
			if(!interestList.contains(interest)) {
				interest.trim();
				interestList.add(interest);
			}
		}
		
		return new UserDetails(userDetailsStrings[0], age, gender, interestList);
	}
	
	/*
	 * This method adds new user details to user details collection.
	 *
	 */
	public static void addUserDetails(UserDetails userDetails) {
		userNameToUserDetailsMap.put(userDetails.getUserName(), userDetails);
		
		// Add user name to age to user name mapping based on gender
		if(userDetails.getGender().equals(Constants.GENDER_MALE)) {
			Set<String> ageToUserNameMaleValueSet = null;
			if(ageToUserNameMale.containsKey(userDetails.getAge())){
				ageToUserNameMaleValueSet = ageToUserNameMale.get(userDetails.getAge());
			} else {
				ageToUserNameMaleValueSet = new HashSet<String>();
				ageToUserNameMale.put(userDetails.getAge(), ageToUserNameMaleValueSet);
			}
			ageToUserNameMaleValueSet.add(userDetails.getUserName());
		} else if (userDetails.getGender().equals(Constants.GENDER_FEMALE)) {
			Set<String> ageToUserNameFemaleValueSet = null;
			if(ageToUserNameFemale.containsKey(userDetails.getAge())){
				ageToUserNameFemaleValueSet = ageToUserNameFemale.get(userDetails.getAge());
			} else {
				ageToUserNameFemaleValueSet = new HashSet<String>();
				ageToUserNameFemale.put(userDetails.getAge(), ageToUserNameFemaleValueSet);

			}
			ageToUserNameFemaleValueSet.add(userDetails.getUserName());
		}
	}
	
	/*
	 * This method returns all user details stored in user details collection.
	 */
	public static Collection<UserDetails> getAllUserDetails(){
		return userNameToUserDetailsMap.values();
	}
	
	/*
	 * This method checks if a given user details is stored in user details collection.
	 */
	public static boolean isUserDetailsAvailable(String userName){
		return userNameToUserDetailsMap.containsKey(userName);
	}
	
	/*
	 * This method removes a given user details from user details collection.
	 */
	public static void removeUserDetails(String userName) {
		UserDetails userDetailsTobeRemoved = userNameToUserDetailsMap.get(userName);
		if(userDetailsTobeRemoved.getGender().equals(Constants.GENDER_FEMALE)) {
			removeValueFromAgeToUserNameMap(userDetailsTobeRemoved, ageToUserNameFemale);
		} else if (userDetailsTobeRemoved.getGender().equals(Constants.GENDER_MALE)) {
			removeValueFromAgeToUserNameMap(userDetailsTobeRemoved, ageToUserNameMale);
		}
		userNameToUserDetailsMap.remove(userName);
	}
	
	/*
	 * This method returns dating recommendation for a given user.
	 */
	public static List<String> getDatingRecommendations(String userName, int numberOfRecommendations) {
		List<String> finalRecommendations = new ArrayList<String>();
		UserDetails userDetails = userNameToUserDetailsMap.get(userName);
		
		// Find closest match in terms of age based on opposite genders
		Map<Integer, List<String>>  ageBasedRecommendations = null;
		if(userDetails.getGender().equals(Constants.GENDER_FEMALE)) {
			ageBasedRecommendations = getClosestAgeRecommendation(userDetails.getAge().intValue(),
																ageToUserNameMale,numberOfRecommendations);
		} else if (userDetails.getGender().equals(Constants.GENDER_MALE)) {
			ageBasedRecommendations = getClosestAgeRecommendation(userDetails.getAge().intValue(),
																ageToUserNameFemale,numberOfRecommendations);
		}
		
		// If the age difference is same then reorder the recommendations based on interests
		SortedSet<Integer> ageDifferenceKeys = new TreeSet<Integer>(ageBasedRecommendations.keySet());
		
		for(Integer ageDifference: ageDifferenceKeys) {
			List<String> recommendationValuesTemp = ageBasedRecommendations.get(ageDifference);
			if(recommendationValuesTemp.size() == 1) {
				// If only one value is there then add it to final recommendation list.
				// No need to further sort the entry based on interest list
				finalRecommendations.addAll(recommendationValuesTemp);
			} else {
				// There are multiple values for same age difference.
				// Hence the values to be sorted based on interest.
				finalRecommendations.addAll(sortRecommendationsListBasedOnInterests(userName, recommendationValuesTemp));
			}
		}
		
		// There may be more number of recommendations due to same age difference or interest count.
		// Hence restrict final recommendation list length to number of recommendations needed.
		if(finalRecommendations.size() > numberOfRecommendations) {
			finalRecommendations = finalRecommendations.subList(0, numberOfRecommendations);
		}
		return finalRecommendations;
	}
	
	/* *************************************************************************
	 * Data structures used to store user details and age based mapping for quick lookup.
	 * *************************************************************************
	 */
	private static Map<String, UserDetails> userNameToUserDetailsMap = new TreeMap<String, UserDetails>(
			new Comparator<String>() {
				@Override
				public int compare(String o1, String o2) {
					return o1.compareTo(o2);//sort in ascending order
				}
				});
	private static Map<Integer, Set<String>> ageToUserNameMale 		 = new HashMap<Integer, Set<String>>();
	private static Map<Integer, Set<String>> ageToUserNameFemale 	 = new HashMap<Integer, Set<String>>();
	
	/* *************************************************************************
	 * Helper methods to be used internally UserDetailsCollection Class
	 * *************************************************************************
	 *
	 * Helper Method: This method returns recommendations for opposite gender for a given age.
	 * 
	 */
	private static Map<Integer, List<String>> getClosestAgeRecommendation(Integer userAge, 
																Map<Integer, Set<String>> ageMapOfOppositeGender, 
																int numberOfRecommendations){
		//List<String> recommendations = new LinkedList<String>();
		Map<Integer, List<String>> ageDifferenceToRecommendationMap = new TreeMap<Integer, List<String>>(
														new Comparator<Integer>() {
															@Override
															public int compare(Integer o1, Integer o2) {
																return o1.compareTo(o2);//sort in ascending order
															}
															});
		
		int recommendationCount = 0;
		int ageDifference = 0;
		
		if(ageMapOfOppositeGender.containsKey(userAge)) {
			Set<String> tempValues = ageMapOfOppositeGender.get(userAge);
			for(String targetUserName: tempValues) {
				//recommendations.add(targetUserName);
				addTargetUserNameToAgeComparisonMap(ageDifference, targetUserName, ageDifferenceToRecommendationMap);
				recommendationCount++;
			}
			
			if(recommendationCount >= numberOfRecommendations) {
				return ageDifferenceToRecommendationMap;
			}
		}
		
		ageDifference += 1;
		int olderAgeForComparison = userAge + ageDifference;
		int youngerAgeForComparison = userAge - ageDifference;
		
		while(youngerAgeForComparison >= Constants.MINIMUM_AGE_FOR_DATING) {
			
			if(ageMapOfOppositeGender.containsKey(olderAgeForComparison)) {
				Set<String> tempValues = ageMapOfOppositeGender.get(olderAgeForComparison);
				for(String targetUserName: tempValues) {
					addTargetUserNameToAgeComparisonMap(ageDifference, targetUserName, ageDifferenceToRecommendationMap);
					recommendationCount++;
				}
			}
			
			if(ageMapOfOppositeGender.containsKey(youngerAgeForComparison)) {
				Set<String> tempValues = ageMapOfOppositeGender.get(youngerAgeForComparison);
				for(String targetUserName: tempValues) {
					addTargetUserNameToAgeComparisonMap(ageDifference, targetUserName, ageDifferenceToRecommendationMap);
					recommendationCount++;
				}
			}
			
			if(recommendationCount >= numberOfRecommendations) {
				return ageDifferenceToRecommendationMap;
			}
			
			ageDifference ++;
			olderAgeForComparison = userAge + ageDifference;
			youngerAgeForComparison = userAge - ageDifference;
		}
		return ageDifferenceToRecommendationMap;
	}
	
	/*
	 * Helper Method: This method returns recommendations for opposite gender based on interests. 
	 * This is expected to be called after list of recommendations is created based on age.
	 * 
	 */
	private static List<String> sortRecommendationsListBasedOnInterests(String sourceUserName, List<String> recommendedValues) {
		Map<Integer, List<String>> interestsBasedRecommendationMap = new TreeMap<Integer, List<String>>(
				new Comparator<Integer>() {
					@Override
					public int compare(Integer o1, Integer o2) {
						return o2.compareTo(o1);//sort in descending order
					}
					});
		
		for(String recommendedValue: recommendedValues) {
			Integer interestMatchCount = getInterestMatchCount(sourceUserName, recommendedValue);
			List<String> sortedRecommendedValuesTemp = null;
			if(interestsBasedRecommendationMap.containsKey(interestMatchCount)) {
				sortedRecommendedValuesTemp = interestsBasedRecommendationMap.get(interestMatchCount);
			} else {
				sortedRecommendedValuesTemp = new ArrayList<String>();
				interestsBasedRecommendationMap.put(interestMatchCount, sortedRecommendedValuesTemp);
			}
			sortedRecommendedValuesTemp.add(recommendedValue);
		}
		
		List<String> interestedBasedRecommendationList = new ArrayList<String>();
		
		// 
		SortedSet<Integer> interestMatchCounts = new TreeSet<Integer>(interestsBasedRecommendationMap.keySet());
		for(Integer matchCount:interestMatchCounts) {
			interestedBasedRecommendationList.addAll(interestsBasedRecommendationMap.get(matchCount));
		}
		
		return interestedBasedRecommendationList;
		
	}
	
	/*
	 * Helper Method: This matches common interests between source user and target user.
	 * 
	 */
	private static Integer getInterestMatchCount(String sourceUserName, String targetUserName) {
		Integer matchCount = 0;
		
		UserDetails sourceUserDetails = userNameToUserDetailsMap.get(sourceUserName);
		UserDetails targetUserDetails = userNameToUserDetailsMap.get(targetUserName);
		
		for(String interest: sourceUserDetails.getInterests()) {
			if(targetUserDetails.getInterests().contains(interest)) {
				matchCount++;
			}
		}
		return matchCount;
	}
	
	/*
	 * Helper Method: This method adds target user name to given age comparison map.
	 * 
	 */
	private static void addTargetUserNameToAgeComparisonMap(Integer ageDifference, String targetUserName, Map<Integer, List<String>> ageDifferenceToRecommendationMap) {
		List<String> recommendationValues = null;
		if(ageDifferenceToRecommendationMap.containsKey(ageDifference)) {
			recommendationValues = ageDifferenceToRecommendationMap.get(ageDifference);
		} else {
			recommendationValues = new ArrayList<String>();
			ageDifferenceToRecommendationMap.put(ageDifference, recommendationValues);
		}
		recommendationValues.add(targetUserName);
	}
	
	/*
	 * Helper Method: This method removes a user from age to user map.
	 * 
	 */
	private static void removeValueFromAgeToUserNameMap(UserDetails userDetailsToBeRemoved, 
														Map<Integer, Set<String>> ageToUserNameMap) {
		
		Set<String> userNameSet = ageToUserNameMap.get(userDetailsToBeRemoved.getAge());
		userNameSet.remove(userDetailsToBeRemoved.getUserName());
		if(userNameSet.isEmpty()){
			ageToUserNameMap.remove(userDetailsToBeRemoved.getAge());
		}
	
	}

}
