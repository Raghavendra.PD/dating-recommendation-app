 Dating Recommendation App demonstrates dating recommendation functionality in Java.
 
 ##################################################################################
 #                        Functional Requirement                                  #
 ##################################################################################
 	Write a Dating recommendation app given the following details:

	Name
	Gender
	Age
	Interests
	
	Preference should be given in the following order:
	Gender -> Opposite gender to be give the preference
	Age -> closest match
	Interests -> Closest match of the list of interests/hobbies 
	
	For e.g. 
	
	Input: (Name-Age-Gender-Interests)
	UserA-25-Female-Cricket,Tennis
	UserB-27-Male-Cricket,Football,Movies
	UserC-26-Male-Movies,Tennis,Football,Cricket
	UserD-24-Female-Tennis,Football,Badminton
	UserE-32-Female-Cricket,Football,Movies,Badminton
	
	If I ask to fetch top 2 matches for User B, the order should be as follows:
	UserA 
	UserD
	
	Explanation: Though UserE & UserB has maximum number of interests matching, age
	is given preference over interests. Similarly though UserB & UserC are closest 
	in terms of age & also all the interests being matched, gender is given 
	preference over age & interests.
	UserA is closest in terms of age when compared to UserD as the interests count
	match hence UserA is given more preference.
####################################################################################


	 
####################################################################################
#                                   Usage                                          #
#################################################################################### 
	'DatingRecommendationApp' the main class. Invoke this to start the app.
	 
	USAGE:
	- Initially the the class asks for user input for following operations
	 				[1] Add user details
	 				[2] Get dating recommendations
	 				[3] Remove user details
	 				[4] Display user details already entered
	 				[5] Exit app.
	 
	 - To add user details (operation 1 mentioned above), user name, age, 
	 	gender and interests to be entered in following format: 
	 		<user name>-<age>-<gender>-<interests>
	 		Example: UserA-25-Female-Cricket,Tennis
	 				
	 - To get dating recommendation (operation 2 mentioned above), user name
	 	and number of recommendation need to be entered.
	 
	 - To remove user details already entered (operation 3 mentioned above), 
	 	user name needs to be entered.
	 	
	Note: The purpose of this app is to demonstrate recommendation generation
	algorithm in Java. Functionality related to rich GUI, user authentication, 
	relational/NoSQL database etc. are not considered in current version.
####################################################################################



####################################################################################
#                  Sample console input and output                                 #
#################################################################################### 
bash-3.2$ java DatingRecommendationApp
================================================================================
Enter a number corresponding to operation given below: 
[1] Add user details
[2] Get dating recommendation for user
[3] Remove user details
[4] Display user database
[5] Exit app
================================================================================
1
--------------------------------------------------------------------------------
Enter user details in the following format: 

<User Name>-<Age>-<Geneder (Male/Female)>-<Comma separated interests>
For Example: User1-25-Male-Cricket,Football,Movies

Enter <DONE> if there are no more user deatils.
--------------------------------------------------------------------------------
UserA-25-Female-Cricket,Tennis
--------------------------------------------------------------------------------
Enter next user details. 
Enter <DONE> if there are no more user deatils.
--------------------------------------------------------------------------------
UserB-27-Male-Cricket,Football,Movies
--------------------------------------------------------------------------------
Enter next user details. 
Enter <DONE> if there are no more user deatils.
--------------------------------------------------------------------------------
UserC-26-Male-Movies,Tennis,Football,Cricket
--------------------------------------------------------------------------------
Enter next user details. 
Enter <DONE> if there are no more user deatils.
--------------------------------------------------------------------------------
UserD-24-Female-Tennis,Football,Badminton
--------------------------------------------------------------------------------
Enter next user details. 
Enter <DONE> if there are no more user deatils.
--------------------------------------------------------------------------------
UserE-32-Female-Cricket,Football,Movies,Badminton
--------------------------------------------------------------------------------
Enter next user details. 
Enter <DONE> if there are no more user deatils.
--------------------------------------------------------------------------------
<DONE>
================================================================================
Enter a number corresponding to operation given below: 
[1] Add user details
[2] Get dating recommendation for user
[3] Remove user details
[4] Display user database
[5] Exit app
================================================================================
4
--------------------------------------------------------------------------------
User name: UserA
Age: 25
Gender: Female
Interests: [Cricket,Tennis]
--------------------------------------------------------------------------------
User name: UserB
Age: 27
Gender: Male
Interests: [Cricket,Football,Movies]
--------------------------------------------------------------------------------
User name: UserC
Age: 26
Gender: Male
Interests: [Movies,Tennis,Football,Cricket]
--------------------------------------------------------------------------------
User name: UserD
Age: 24
Gender: Female
Interests: [Tennis,Football,Badminton]
--------------------------------------------------------------------------------
User name: UserE
Age: 32
Gender: Female
Interests: [Cricket,Football,Movies,Badminton]
--------------------------------------------------------------------------------
================================================================================
Enter a number corresponding to operation given below: 
[1] Add user details
[2] Get dating recommendation for user
[3] Remove user details
[4] Display user database
[5] Exit app
================================================================================
2
--------------------------------------------------------------------------------
Enter user name: 
--------------------------------------------------------------------------------
UserB
--------------------------------------------------------------------------------
Enter number of recommendations needed: 
--------------------------------------------------------------------------------
2
--------------------------------------------------------------------------------
Dating recommendations for UserB:
UserA
UserD
--------------------------------------------------------------------------------
================================================================================
Enter a number corresponding to operation given below: 
[1] Add user details
[2] Get dating recommendation for user
[3] Remove user details
[4] Display user database
[5] Exit app
================================================================================
1
--------------------------------------------------------------------------------
Enter user details in the following format: 

<User Name>-<Age>-<Geneder (Male/Female)>-<Comma separated interests>
For Example: User1-25-Male-Cricket,Football,Movies

Enter <DONE> if there are no more user deatils.
--------------------------------------------------------------------------------
UserF-27-Female-Cricket,Football,Movies
--------------------------------------------------------------------------------
Enter next user details. 
Enter <DONE> if there are no more user deatils.
--------------------------------------------------------------------------------
<DONE>
================================================================================
Enter a number corresponding to operation given below: 
[1] Add user details
[2] Get dating recommendation for user
[3] Remove user details
[4] Display user database
[5] Exit app
================================================================================
2
--------------------------------------------------------------------------------
Enter user name: 
--------------------------------------------------------------------------------
UserB
--------------------------------------------------------------------------------
Enter number of recommendations needed: 
--------------------------------------------------------------------------------
3
--------------------------------------------------------------------------------
Dating recommendations for UserB:
UserF
UserA
UserD
--------------------------------------------------------------------------------
================================================================================
Enter a number corresponding to operation given below: 
[1] Add user details
[2] Get dating recommendation for user
[3] Remove user details
[4] Display user database
[5] Exit app
================================================================================
		   