/*
 * This is a helper class containing static strings, delimiters and integer values.
 */
public class Constants {
	
	public static final int OPERATION_ADD_USER_DETAILS = 1;
	
	public static final int OPERATION_GET_DATING_RECOMMENDATION = 2;
	
	public static final int OPERATION_REMOVE_USER_DETAILS = 3;

	public static final int OPERATION_EXIT_APP = 5;
	
	public static final int OPERATION_DISPLAY_USER_DB = 4;

	public static final String USER_DETAILS_DELIMETER = "-";
	
	public static final String END_OF_USER_INPUT = "<DONE>";
	
	public static final String TOP_LEVEL_LINE_BREAK = "================================================================================\n";

	public static final String INTERMEDIATE_LINE_BREAK = "--------------------------------------------------------------------------------\n";
	
	public static final String OPERATION_INPUT_REQUEST_MESSAGE = TOP_LEVEL_LINE_BREAK
			+ "Enter a number corresponding to operation given below: \n"
			+ "[" + OPERATION_ADD_USER_DETAILS +"] Add user details\n"
			+ "[" + OPERATION_GET_DATING_RECOMMENDATION + "] Get dating recommendation for user\n"
			+ "[" + OPERATION_REMOVE_USER_DETAILS + "] Remove user details\n"
			+ "[" + OPERATION_DISPLAY_USER_DB + "] Display user database\n"
			+ "[" + OPERATION_EXIT_APP + "] Exit app\n"
			+ TOP_LEVEL_LINE_BREAK;
	
	public static final String FIRST_USER_INPUT_REQUEST_MESSAGE = INTERMEDIATE_LINE_BREAK
			+ "Enter user details in the following format: \n\n"
			+ "<User Name>" + USER_DETAILS_DELIMETER
			+ "<Age>" + USER_DETAILS_DELIMETER
			+ "<Geneder (Male/Female)>" + USER_DETAILS_DELIMETER
			+ "<Comma separated interests>\n"
			+ "For Example: "
			+ "User1" + USER_DETAILS_DELIMETER
			+ "25" + USER_DETAILS_DELIMETER
			+ "Male" + USER_DETAILS_DELIMETER
			+ "Cricket,Football,Movies\n\n"
			+ "Enter " + END_OF_USER_INPUT  + " if there are no more user deatils.\n"
			+ INTERMEDIATE_LINE_BREAK;
	
	public static final String NEXT_USER_INPUT_REQUEST_MESSAGE = INTERMEDIATE_LINE_BREAK
			+ "Enter next user details. \n"
			+ "Enter " + END_OF_USER_INPUT  + " if there are no more user deatils.\n"
			+ INTERMEDIATE_LINE_BREAK;
	
	public static final String DATING_RECOMMENDATION_INPUT_MESSAGE = INTERMEDIATE_LINE_BREAK
			+ "Enter user name: \n"
			+ INTERMEDIATE_LINE_BREAK;
	
	public static final String DATING_RECOMMENDATION_INPUT_MESSAGE_NO_OF_RECOMM = INTERMEDIATE_LINE_BREAK
			+ "Enter number of recommendations needed: \n"
			+ INTERMEDIATE_LINE_BREAK;
	
	public static final String USER_REMOVAL_INPUT_MESSAGE = INTERMEDIATE_LINE_BREAK
			+ "Enter user name to be removed: \n"
			+ INTERMEDIATE_LINE_BREAK;
		
	public static final String GENDER_MALE = "Male";
	
	public static final String GENDER_FEMALE = "Female";
	
	public static final String INTERESTS_INPUT_DELIMITER = "-";
	
	public static final int MINIMUM_AGE_FOR_DATING = 18;


}
